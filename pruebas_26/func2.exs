defmodule Impuestos do

  # ***FUNCION PÚBLICA, LLAMANDO AL MODULO PUNTO FUNCION
  # def total(precio, tipo)do
  #  ( 1+ Impuestos.porcentaje(tipo)) *precio
  # end

  # ***FUNCION PUBLICA LLAMANDO DIRECTAMENTE AL MODULO
  def pruebaTotal(precio, tipo) do
  (1 + porcentaje(tipo)) * precio
  end

  # *** PARA CREAR FUNCIONES PRIVADAS(solo pueden ser llamadas dentro de lmodulo, se cambia el DEF por DEFP)
  defp porcentaje(tipo) do
    cond do
      tipo == :normal -> 0.21
      tipo == :reducido -> 0.10
      tipo == :super_reducido -> 0.04
    end
  end
end

# IO.inspect Impuestos.pruebaTotal(10, :reducido )
IO.inspect Impuestos.porcentaje(:normal)
