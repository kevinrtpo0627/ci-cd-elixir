- El operador igual '=', no asigna sino que proviene del match de pattern matching

i-   x = 5, elixir resuelve la ecuación.

Commands into IEX

-   x = 4+6
    {:hello, :goodbye} == {:hello, :world}
    ...false

-    {:hello, :goodbye} == {:hello, :goodbye} 
    ... true

Entiendo que esto funciona como la d  estructuración en JS, al hacer una asignación lo que se hace es ordenar solucionar una igualdad.

- {1,a,3} = {1,2,3}
- {0, texto} = {0, "hola mundo"}

- {true, texto} = {false, "fallo pattern matching"}
  ......error

