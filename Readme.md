Guardas: Son condiciones que se ponen en la definicion de las funciones para indicar cuando ejecutar esa función, por ejemplo:
    - def dividir(a,b)do
        a/b
      end

    - def dividir(a,b) when b=0 do
        :inf
      end

    Si no se coloca ninguna guarda se asume un 'when true'

Aridad: Indica el número de parametros que requiere la función, si se crean funciones con el mismo nombre pero diferente aridad, Elixir escogera el que coincida.